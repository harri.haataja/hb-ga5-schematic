# HB GA5 schematic

Schematic of Harley Benton GA5 tube guitar amplifier head. Drawn for the
purpose of making notes of modifications. This is not a guide or recommendation
but ideas are free to take and welcome to leave. Fork for your own use if you
like.

There may naturally be errors and version differences.

At least one branch will be based purely on my own unit and may not in any way
resemble any other GA5 or Valve Jr in existence.

## License

TBD, probably CC with no responsibility or demands.

## Mods

1. Moved R1 to input jack side
2. Swapped R1 and R6
3. Swapped 300 (298) ohm resistor for R14
4. Swapped to 240 V mains
5. Added screen resistor, 1k
7. Added fuses on 12V outputs and wires out
8. Made a Framus tone control board [Calculator link](https://www.guitarscience.net/tsc/framus.htm#RIN=220k&R1=47k&RM=500k&RL=64.9k&C1=2.2n&C2=2.2n&RM_pot=Linear)
9. Swapped C3 for 0.68u


## Notes

* Cutting JP1 or changing to a switch is often suggested as first mod
* Compare to Valve Special circuit and mods and test freq response

## Links

* <https://www.freestompboxes.org/viewtopic.php?t=2203>
* <https://en.wikipedia.org/wiki/Epiphone_Valve_Junior>
* <http://www.sewatt.com/>
* <https://robrobinette.com/Generic_Tube_Amp_Mods.htm#Framus_Mid_Control>
* <https://www.freestompboxes.org/viewtopic.php?t=2203>
* <http://duhvoodooman.com/VJr/VJr_mods.htm>
* <https://web.archive.org/web/20190917191333/http://www.erikmiller.users.sonic.net/Euthymia/DIY/VJMods.html>
* <https://www.freestompboxes.org/viewtopic.php?f=2&t=1920>
* <https://www.tropicalfishvintage.com/blog/2019/5/26/gibson-ga-5-vs-fender-5f1-circuit-analysis>
* <https://www.tropicalfishvintage.com/blog/2017/6/21/what-is-this-gibson-ga-5-and-why-does-it-remind-me-of-a-fender-champ>

